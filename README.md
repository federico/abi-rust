This module builds two shared libraries, `libone.so` and `libtwo.so`.
Both have a `Foo` struct, and a `foo()` function.  The structs have
different fields, and the functions take different arguments.  It
should be possible to get the differences in ABI between these two
libraries.

Just do `cargo build` in the toplevel; this should build both
libraries an place them under `target/`.
