#[repr(C)]
pub struct Foo {
    a: u32,
    b: u32,
}

#[no_mangle]
pub unsafe extern fn foo(a: u32, b: u32) -> Foo {
    Foo {
        a,
        b,
    }
}
