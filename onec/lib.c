#include <stdint.h>

struct Foo {
    uint32_t a;
};

struct Foo foo(uint32_t a) {
    struct Foo foo;
    foo.a = a;

    return foo;
}

